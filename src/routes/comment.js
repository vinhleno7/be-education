const router = require("express").Router();
const commentCtrl = require("../controllers/commentCtrl");
const auth = require("../middleware/auth");

router.post("/createComment", auth, commentCtrl.createComment);
router.delete("/delete/:id", auth, commentCtrl.delete);
router.get("/getAllComments", commentCtrl.getAllComments);
module.exports = router;
