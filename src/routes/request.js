const router = require("express").Router();
const requestCtrl = require("../controllers/requestCtrl");
const auth = require("../middleware/auth");

router.post("/create", auth, requestCtrl.create);
router.get("/getAllRequest", requestCtrl.getAllRequest);
module.exports = router;
