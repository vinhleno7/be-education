const router = require("express").Router();
const classCtrl = require("../controllers/classCtrl");
const auth = require("../middleware/auth");

router.post("/insert", auth, classCtrl.insert);

router.get("/getAll", auth, classCtrl.getAllClass);

router.delete("/deleteClass/:id", classCtrl.deleteClass);

router.put("/updateClass/:id", classCtrl.updateClass);

router.get("/getAnClass/:id", classCtrl.getAnClass);

module.exports = router;
