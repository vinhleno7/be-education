const router = require("express").Router();
const subjectCtrl = require("../controllers/subjectCtrl");
const auth = require("../middleware/auth");

router.post("/insert", auth, subjectCtrl.insert);

router.delete("/delete/:id", subjectCtrl.delete);

router.put("/update/:id", subjectCtrl.update);

router.get("/getAllSubject", subjectCtrl.getAllSubject);

router.get("/getASubject/:id", subjectCtrl.getASubject);

module.exports = router;
