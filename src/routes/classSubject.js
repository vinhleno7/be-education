const router = require("express").Router();
const classSubjectCtrl = require("../controllers/classSubject");
const auth = require("../middleware/auth");

router.post("/insert", auth, classSubjectCtrl.insert);

router.post("/addStudent", auth, classSubjectCtrl.addStudents);

router.post("/deleteStudent", auth, classSubjectCtrl.deleteStudent);

router.delete("/delete/:id", auth, classSubjectCtrl.deleteSubjectClass);

router.put("/update/:id", auth, classSubjectCtrl.updateSubjectClass);

router.get("/getAllSubjectClass", classSubjectCtrl.getAllSubjectClass);

router.get("/getASubjectClass/:id", classSubjectCtrl.getASubjectClass);

router.get("/getCSByIdUser", auth, classSubjectCtrl.getCSByIdUser);

module.exports = router;
