const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const postCtrl = require("../controllers/postCtrl");

router.post("/insert", auth, postCtrl.insert);

router.route("/updatePost/:id").patch(auth, postCtrl.update);

router.get("/getPosts/:id", auth, postCtrl.getPosts);
router.get("/getAllPosts", postCtrl.getAllPosts);

router.delete("/delete/:id", auth, postCtrl.delete);
module.exports = router;
