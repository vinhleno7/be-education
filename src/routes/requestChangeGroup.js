const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const requestChangeGroupCtrl = require("../controllers/requestChangeGroup");

router.post("/create", auth, requestChangeGroupCtrl.create);
router.get("/getAllByIdUser/:id", auth, requestChangeGroupCtrl.getAllByIdUser);
router.get("/getAll", auth, requestChangeGroupCtrl.getAll);
router.post("/reply", auth, requestChangeGroupCtrl.reply);

module.exports = router;
