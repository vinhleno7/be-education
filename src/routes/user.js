const router = require("express").Router();
const auth = require("../middleware/auth");
const userCtrl = require("../controllers/userCtrl");

router.get("/user/:id", auth, userCtrl.getUser);

router.get("/getAllUser", auth, userCtrl.getAllUser);

router.patch("/user", auth, userCtrl.updateUser);

router.delete("/user/:id", auth, userCtrl.deleteUser);

module.exports = router;
