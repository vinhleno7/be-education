const router = require("express").Router();
const majorCtrl = require("../controllers/majorCtrl");
const auth = require("../middleware/auth");

router.post("/insert", auth, majorCtrl.insert);

router.get("/getAll", auth, majorCtrl.getAll);

router.delete("/deleteMajor/:id", majorCtrl.deleteMajor);

router.put("/updateMajor/:id", majorCtrl.updateMajor);

router.get("/getAnMajor/:id", majorCtrl.getAnMajor);

module.exports = router;
