const router = require("express").Router();
const replyCtrl = require("../controllers/replyCtrl");
const auth = require("../middleware/auth");

router.post("/create", auth, replyCtrl.create);
router.delete("/delete/:id",  replyCtrl.delete);
module.exports = router;
