const router = require("express").Router();
const groupSubjectCtrl = require("../controllers/groupSubjectCtrl");
const auth = require("../middleware/auth");

router.post("/insert", auth, groupSubjectCtrl.insertGroup);
router.post("/addMember", auth, groupSubjectCtrl.addMembers);
router.post("/deleteMember", auth, groupSubjectCtrl.deleteMember);
router.get("/getAllGroup", auth, groupSubjectCtrl.getAllGroup);
router.get("/getAGroup/:id", auth, groupSubjectCtrl.getAGroup);
router.delete("/deleteGroup/:id", auth, groupSubjectCtrl.deleteGroup);
router.patch("/updateGroup", auth, groupSubjectCtrl.updateGroup);

module.exports = router;
