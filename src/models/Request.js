const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const requestSchema = new Schema(
  {
    // title: {
    //   type: String,
    //   required: true
    // },
    content: {
      type: String,
      required: true
    },
    replies: [{ type: mongoose.Types.ObjectId, ref: "reply" }],
    classSubject: { type: mongoose.Types.ObjectId, ref: "classSubject" },
    student: { type: mongoose.Types.ObjectId, ref: "user" },
    teacher: { type: mongoose.Types.ObjectId, ref: "user" }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("request", requestSchema);
