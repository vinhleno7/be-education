const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const requestGroupSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    group: { type: mongoose.Types.ObjectId, ref: "group" },
    classSubject: { type: mongoose.Types.ObjectId, ref: "classSubject" },
    groupWantChange: { type: mongoose.Types.ObjectId, ref: "group" },
    student: { type: mongoose.Types.ObjectId, ref: "user" },
    teacher: { type: mongoose.Types.ObjectId, ref: "user" },
    status: {
      type: String,
      enum: ["yes", "no"]
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("requestGroup", requestGroupSchema);
