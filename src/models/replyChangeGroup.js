const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ReplyGroupSchema = new Schema(
  {
    content: {
      type: String,
      required: true
    },
    status: {
      type: Boolean
    },
    requestID: {
      type: String
    },

    author: [{ type: mongoose.Types.ObjectId, ref: "user" }],
    saved: [{ type: mongoose.Types.ObjectId, ref: "replyGroups" }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("replyGroups", ReplyGroupSchema);
