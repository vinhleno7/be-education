const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ClassSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      upperCase: true,
      required: true,
      trim: true
    },
    users: [{ type: mongoose.Types.ObjectId, ref: "user" }],
    major: { type: mongoose.Types.ObjectId, ref: "major" }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("class", ClassSchema);
