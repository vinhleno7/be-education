const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    fullName: {
      type: String,
      required: true,
      maxlength: 25
    },
    studentID: {
      type: String,
      required: true,
      trim: true,
      maxlength: 10,
      unique: true
    },
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    avatar: {
      type: String,
      default:
        "https://res.cloudinary.com/devatchannel/image/upload/v1602752402/avatar/avatar_cugq40.png"
    },
    role: {
      type: String,
      default: "student",
      enum: ["student", "teacher", "admin"]
    },
    class: { type: mongoose.Types.ObjectId, ref: "class" },
    major: { type: mongoose.Types.ObjectId, ref: "major" },
    gender: {
      type: String,
      default: "male",
      enum: ["male", "female", "other"]
    },
    mobile: { type: String, default: "" },
    story: {
      type: String,
      default: "",
      maxlength: 200
    },
    website: { type: String, default: "" },
    classSubjects: [{ type: mongoose.Types.ObjectId, ref: "classSubject" }]
    // groups: [{ type: mongoose.Types.ObjectId, ref: "group" }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("user", UserSchema);

// studentID.length ==10
// stuCheck ~~ email must be stu 's mail
// role must be one of [student,cbgd,admin]
// password >6
//
