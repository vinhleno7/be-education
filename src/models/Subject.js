const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const SubjectSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true
    },

    major: { type: mongoose.Types.ObjectId, ref: "major" },

    schoolYear: {
      type: String,
      required: true
    },

    note: {
      type: String,
      default: "",
      maxlength: 200
    },
    classSubjects: [{ type: mongoose.Types.ObjectId, ref: "classSubject" }]
    // teachers: [{ type: mongoose.Types.ObjectId, ref: "user" }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("subject", SubjectSchema);
