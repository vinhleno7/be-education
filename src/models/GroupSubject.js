const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const GroupSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    members: [{ type: mongoose.Types.ObjectId, ref: "user" }],
    classSubject: { type: mongoose.Types.ObjectId, ref: "classSubject" },
    saved: [{ type: mongoose.Types.ObjectId, ref: "group" }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("group", GroupSchema);
