const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ClassSubjectSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },

    schoolYear: {
      type: String,
      required: true
    },

    note: {
      type: String,
      default: "",
      maxlength: 200
    },

    subject: { type: mongoose.Types.ObjectId, ref: "subject" },

    groups: [{ type: mongoose.Types.ObjectId, ref: "group" }],

    teacher: { type: mongoose.Types.ObjectId, ref: "user" },

    students: [{ type: mongoose.Types.ObjectId, ref: "user" }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("classSubject", ClassSubjectSchema);
