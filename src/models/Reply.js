const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ReplySchema = new Schema(
  {
    content: {
      type: String,
      required: true
    },

    // replyComment: mongoose.Types.ObjectId,
    author: { type: mongoose.Types.ObjectId, ref: "user" },
    requestID: { type: mongoose.Types.ObjectId, ref: "request" }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("reply", ReplySchema);
