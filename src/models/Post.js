const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const PostSchema = new Schema(
  {
    content: {
      type: String,
      required: true
    },

    images: {
      type: Array
    },
    // replies: [{ type: mongoose.Types.ObjectId, ref: "post" }],

    // status: {
    //   type: String
    //   // enum: ["TO LEARN", "LEARNING", "LEARNED"]
    // },
    author: {
      type: Schema.Types.ObjectId,
      ref: "user"
    },
    classSubject: {
      type: Schema.Types.ObjectId,
      ref: "classSubject"
    },
    group: {
      type: Schema.Types.ObjectId,
      ref: "group"
    },
    partner: { type: Schema.Types.ObjectId, ref: "user" },
    comments: [{ type: mongoose.Types.ObjectId, ref: "comment" }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("post", PostSchema);
