const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MajorSchema = new Schema(
  {
    majorID: { type: String, required: true, unique: true, upperCase: true },
    name: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("major", MajorSchema);
