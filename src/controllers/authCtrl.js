const mongoose = require("mongoose");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const authCtrl = {
  register: async (req, res) => {
    console.log("REGISTER...");
    const Classes = mongoose.model("class");
    const Majors = mongoose.model("major");
    try {
      const { fullName, studentID, email, password, classID, majorID } =
        req.body;
      let newStudentID = studentID.toLowerCase().replace(/ /g, "");

      const student_id = await User.findOne({ studentID: newStudentID });
      console.log(
        "🚀 ~ file: authCtrl.js ~ line 17 ~ register: ~ student_id",
        student_id
      );

      if (student_id)
        return res.status(400).json({ msg: "This user ID already exists." });
      if (studentID.length != 10)
        return res
          .status(400)
          .json({ msg: "This studentID must be 10 characters" });

      const user_email = await User.findOne({ email });
      if (user_email)
        return res.status(400).json({ msg: "This email already exists." });
      const domain = email.substring(email.lastIndexOf("@") + 1);
      const stuCheck = domain.includes("stu");
      if (!stuCheck) {
        return res.status(400).json({ msg: "This Email doesn't STU 's email" });
      }
      const newRoleTempt = domain.substring(0, domain.indexOf("."));

      let newRole;
      let newAvatar;
      if (newRoleTempt == "student") {
        newRole = "student";
        newAvatar =
          "https://res.cloudinary.com/lv-group/image/upload/v1650866746/education/student_xkcz9g.png";
      }
      if (newRoleTempt == "cbgd") {
        newRole = "teacher";
        newAvatar =
          "https://res.cloudinary.com/lv-group/image/upload/v1650866750/education/teacher_ncogp2.jpg";
      }
      if (newRoleTempt == "admin") {
        newRole = "admin";
        newAvatar =
          "https://res.cloudinary.com/devatchannel/image/upload/v1602752402/avatar/avatar_cugq40.png";
      }
      if (password.length < 6)
        return res
          .status(400)
          .json({ msg: "Password must be at least 6 characters." });

      const myClass = await Classes.findById(classID);

      const myMajor = await Majors.findById(majorID);

      const passwordHash = await bcrypt.hash(password, 12);
      const newUser = new User({
        fullName,
        studentID: newStudentID,
        email,
        password: passwordHash,
        role: newRole,
        class: myClass,
        major: myMajor,
        avatar: newAvatar
      });

      const access_token = createAccessToken({ id: newUser._id });
      const refresh_token = createRefreshToken({ id: newUser._id });

      res.cookie("refreshtoken", refresh_token, {
        httpOnly: true,
        path: "/api/refresh_token",
        maxAge: 30 * 24 * 60 * 60 * 1000 // 30days
      });

      await Classes.findOneAndUpdate(
        {
          _id: classID
        },
        {
          $push: { users: newUser._id }
        },
        { new: true }
      );

      await newUser.save();

      res.json({
        msg: "Register Success!",
        access_token,
        user: {
          ...newUser._doc,
          password: ""
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

  login: async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ email });

      if (!user)
        return res.status(400).json({ msg: "This email does not exist." });

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch)
        return res.status(400).json({ msg: "Password is incorrect." });

      const Classes = mongoose.model("class");
      const Majors = mongoose.model("major");
      const ClassSubjects = mongoose.model("classSubject");

      const myClass = await Classes.findOne({ _id: user.class });
      const myMajor = await Majors.findOne({ _id: user.major });
      const myClassSubject = await ClassSubjects.find({
        _id: user.classSubjects
      });

      const access_token = createAccessToken({ id: user._id });
      const refresh_token = createRefreshToken({ id: user._id });

      res.cookie("refreshtoken", refresh_token, {
        httpOnly: true,
        path: "/api/refresh_token",
        maxAge: 30 * 24 * 60 * 60 * 1000 // 30days
      });

      res.json({
        msg: "Login Success!",
        access_token,
        user: {
          ...user._doc,
          class: myClass,
          major: myMajor,
          classSubjects: myClassSubject,
          password: ""
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  logout: async (req, res) => {
    try {
      res.clearCookie("refreshtoken", { path: "/api/refresh_token" });
      return res.json({ msg: "Logged out!" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  generateAccessToken: async (req, res) => {
    try {
      const rf_token = req.cookies.refreshtoken;

      if (!rf_token) return res.status(400).json({ msg: "Please login now." });

      jwt.verify(
        rf_token,
        process.env.REFRESH_TOKEN_SECRET,
        async (err, result) => {
          if (err) return res.status(400).json({ msg: "Please login now." });

          const user = await User.findById(result.id)
            .select("-password")
            .populate("classSubject group");

          if (!user)
            return res.status(400).json({ msg: "This does not exist." });

          const access_token = createAccessToken({ id: result.id });

          res.json({
            access_token,
            user
          });
        }
      );
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

const createAccessToken = (payload) => {
  return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "1d"
  });
};

const createRefreshToken = (payload) => {
  return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: "30d"
  });
};

module.exports = authCtrl;
