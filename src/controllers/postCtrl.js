const mongoose = require("mongoose");
const Posts = require("../models/Post");
const Comments = require("../models/Comment");
const Users = require("../models/User");
const Groups = require("../models/GroupSubject");

const postCtrl = {
  insert: async (req, res) => {
    const Users = mongoose.model("user");
    const ClassSubjects = mongoose.model("classSubject");
    const Groups = mongoose.model("group");
    try {
      const { content, classSubjectID, groupID, userID } = req.body;
      const myAuthor = await Users.findById(req.user._id);
      let myClassSubject;
      if (classSubjectID != "") {
        myClassSubject = await ClassSubjects.findById(classSubjectID);
      }
      let myGroup;
      if (groupID !== "") {
        myGroup = await Groups.findOne({
          _id: groupID,
          members: req.user._id,
        });
      }
      let myPartner;
      if (userID !== "") {
        myPartner = await Users.findById(userID);
      }

      let newPost;

      if (myClassSubject) {
        newPost = new Posts({
          content,

          author: myAuthor,
          classSubject: myClassSubject,
        });
      }

      if (myGroup) {
        newPost = new Posts({
          content,
          classSubject: myClassSubject,

          author: myAuthor,
          group: myGroup,
        });
      }
      if (myPartner) {
        newPost = new Posts({
          classSubject: myClassSubject,

          content,
          author: myAuthor,
          partner: myPartner,
        });
      }

      await newPost.save();

      res.json({
        msg: "Created Post!",
        newPost: {
          ...newPost._doc,
        },
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

  getPosts: async (req, res) => {
    try {
      const classSubjectID = req.params.id;

      const myGroup = await Groups.findOne({
        classSubject: classSubjectID,
        members: req.user._id,
      });

      const CSs = mongoose.model("classSubject");
      const myCS = await CSs.findOne({ _id: classSubjectID });

      if (req.user.role === "teacher") {
        const postsOfCS = await Posts.find({
          $or: [
            { classSubject: [classSubjectID], group: null },
            { classSubject: [classSubjectID], partner: req.user._id },
            { classSubject: [classSubjectID], author: req.user._id },
          ],
        })
          .sort("-createdAt")
          .populate("author comments classSubject group partner ")
          .populate({
            path: "comments",
            populate: {
              path: "author",
            },
          });
        res.json({
          msg: "Success get post in subjectClass!",
          result: postsOfCS.length,
          postsOfCS,
        });
      } else if (myGroup) {
        const postsOfCS = await Posts.find({
          $or: [
            { classSubject: [classSubjectID], group: null },

            { classSubject: [classSubjectID], group: myGroup._id },
            { classSubject: [classSubjectID], author: req.user._id },
          ],
        })
          .sort("-createdAt")
          .populate("author comments classSubject group partner ")
          .populate({
            path: "comments",
            populate: {
              path: "author",
            },
          });

        res.json({
          msg: "Success get post in subjectClass!",
          result: postsOfCS.length,
          postsOfCS,
        });
      } else {
        const postsOfCS = await Posts.find({
          $or: [
            { classSubject: [classSubjectID], group: null },

            { classSubject: [classSubjectID], author: req.user._id },
          ],
        })
          .sort("-createdAt")
          .populate("author comments classSubject group partner ")
          .populate({
            path: "comments",
            populate: {
              path: "author",
            },
          });

        res.json({
          msg: "Success get post in subjectClass!",
          result: postsOfCS.length,
          postsOfCS,
        });
      }

      // const postsOfGroup = await Posts.find({
      //   group: myGroup._id
      // })
      //   .sort("-createdAt")
      //   .populate("author comments classSubject group ")
      //   .populate({
      //     path: "comments",
      //     populate: {
      //       path: "author"
      //     }
      //   });

      // console.log("postsOfAll:", postsOfCS.length);
      // console.log("postsOfGroup:", postsOfGroup.length);

      // }
      // if (groupID) {
      //   const postsOfGroup = await Posts.find({
      //     group: [groupID]
      //   })
      //     .sort("-createdAt")
      //     .populate("group", "classSubject");

      //   if (groupID) {
      //     res.json({
      //       msg: "Success get post in group!",
      //       result: postsOfGroup.length,
      //       postsOfGroup
      //     });
      //   }
      // }
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  delete: async (req, res) => {
    try {
      const post = await Posts.findOneAndDelete({
        _id: req.params.id,
        author: req.user._id,
      });

      await Comments.deleteMany({ _id: { $in: post.comments } });

      res.json({
        msg: "Deleted Post!",
        newPost: {
          ...post,
          author: req.user,
        },
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  //UPDATE
  update: async (req, res) => {
    const { content } = req.body;
    if (!content)
      return res
        .status(400)
        .json({ msg: "Vui lòng nhập nội dung lời phản hồi" });
    const post = await Posts.findOneAndUpdate(
      { _id: req.params.id },
      { content }
    )
      .populate("author group partner comments")
      .populate({
        path: "comments",
        populate: {
          path: "author",
        },
      });
    res.json({
      msg: "Sửa bài thảo luận thành công !",
      newPost: { ...post._doc, content },
    });
  },
  // updatePost: async (req, res) => {
  //   try {
  //     const { content, images } = req.body;
  //     const post = await Posts.findOneAndUpdate(
  //       { _id: req.params.id },
  //       { content, images }
  //     ).populate("user likes ", "avatar username fullname ");
  //     res.json({
  //       msg: "Update Post!",
  //       newPost: { ...post._doc, content, images }
  //     });
  //   } catch (err) {
  //     return res.status(500).json({ msg: err.message });
  //   }
  // },
  getAllPosts: async (req, res) => {
    try {
      const post = await Posts.find();
      res.status(200).json(post);
    } catch (err) {
      res.status(500).json(err);
    }
  },
};

module.exports = postCtrl;
