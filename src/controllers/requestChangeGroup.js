const mongoose = require("mongoose");
const RequestChangeGroups = require("../models/RequestChangeGroup");
const Users = require("../models/User");

const requestChangeGroupCtrl = {
  create: async (req, res) => {
    const {
      title,
      content,
      groupID,
      classSubjectID,
      groupToChangeID,
      studentID,
      teacherID,
    } = req.body;

    const Groups = mongoose.model("group");
    const ClassSubjects = mongoose.model("classSubject");
    const Students = mongoose.model("user");
    const Teachers = mongoose.model("user");
    try {
      if (!title) return res.status(400).json({ msg: "Vui lòng điền title" });

      if (!groupID)
        return res.status(400).json({ msg: "Vui lòng điền groupID" });
      const checkGroup = await Groups.find({
        _id: groupID,
        classSubject: classSubjectID,
      });

      if (checkGroup.length <= 0)
        return res.status(400).json({ msg: "Nhóm này không hợp lệ !" });

      if (!classSubjectID)
        return res.status(400).json({ msg: "Vui lòng điền classSubjectID" });

      if (!groupToChangeID)
        return res.status(400).json({ msg: "Vui lòng chọn nhóm muốn chuyển." });
      const checkGroup2 = await Groups.find({
        _id: groupToChangeID,
        classSubject: classSubjectID,
      });
      if (!content)
        return res
          .status(400)
          .json({ msg: "Vui lòng điền lý do muốn chuyển nhóm." });
      if (checkGroup2.length <= 0)
        return res.status(404).json({ msg: "Nhóm muốn chuyển không tồn tại" });
      if (!studentID)
        return res.status(400).json({ msg: "Vui lòng điền studentID" });

      const checkStudent = await Groups.find({
        members: studentID,
      });

      if (checkStudent.length <= 0)
        return res
          .status(400)
          .json({ msg: "sinh viên này không có trong group " });

      if (!teacherID)
        return res.status(400).json({ msg: "Vui lòng điền teacherID" });

      const checkCS = await ClassSubjects.find({
        _id: classSubjectID,
        teacher: teacherID,
      });

      if (checkCS.length <= 0)
        return res.status(404).json({ msg: "Thầy giáo này không hợp lệ. " });

      const newRequestChangeGroup = new RequestChangeGroups({
        title,
        content,
        group: groupID,
        classSubject: classSubjectID,
        groupWantChange: groupToChangeID,
        student: studentID,
        teacher: teacherID,
      });

      await newRequestChangeGroup.save();
      const myStudent = await Students.find({ _id: studentID });
      const myTeacher = await Teachers.find({ _id: teacherID });
      res.json({
        msg: "Tạo yêu cầu chuyển nhóm thành công !",
        requestChangeGroup: {
          ...newRequestChangeGroup._doc,
          group: checkGroup,
          classSubject: checkCS,
          groupWantChange: checkGroup2,
          teacher: myTeacher,
          student: myStudent,
        },
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  reply: async (req, res) => {
    const { requestChangeGroupID, status } = req.body;

    try {
      if (!requestChangeGroupID)
        return res
          .status(400)
          .json({ msg: "Vui lòng điền requestChangeGroupID" });

      const checkTeacher = await Users.findOne({
        _id: req.user._id,
        role: "teacher",
      });
      if (!checkTeacher)
        return res.status(400).json({ msg: "Bạn không phải là thầy giáo" });

      if (!status) return res.status(400).json({ msg: "Vui lòng điền status" });

      const myRequest = await RequestChangeGroups.find({
        _id: requestChangeGroupID,
      }).populate("group groupWantChange");

      if (myRequest.length <= 0)
        return res
          .status(404)
          .json({ msg: "Không tồn tại đơn chuyển nhóm này" });

      await RequestChangeGroups.findOneAndUpdate(
        {
          _id: requestChangeGroupID,
        },
        {
          status,
        },
        { new: true }
      );

      if (status === "no")
        return res
          .status(200)
          .json({ msg: "Thầy giáo đã từ chối đơn chuyển nhóm.", status });
      if (status === "yes") {
        const updateGroupMembers = mongoose.model("group");
        await updateGroupMembers.findOneAndUpdate(
          {
            _id: myRequest[0].group._id,
          },
          {
            $pull: { members: myRequest[0].student },
          },
          { new: true }
        );

        await updateGroupMembers.findOneAndUpdate(
          {
            _id: myRequest[0].groupWantChange._id,
          },
          {
            $push: { members: myRequest[0].student },
          },
          { new: true }
        );
      }

      res.status(200).json({
        msg: "Thầy giáo đã đồng ý đơn chuyển nhóm. Nhóm sẽ tự cập nhật. ",
        status,
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllByIdUser: async (req, res) => {
    try {
      const myRequestGroupStudent = await RequestChangeGroups.find({
        classSubject: req.params.id,
        student: req.user._id,
      })
        .sort("-createdAt")
        .populate("student teacher");
      const myRequestGroupTeacher = await RequestChangeGroups.find({
        classSubject: req.params.id,
        teacher: req.user._id,
      })
        .sort("-createdAt")
        .populate("student teacher");

      if (req.user.role === "student") {
        res.status(200).json(myRequestGroupStudent);
      }
      if (req.user.role === "teacher") {
        res.status(200).json(myRequestGroupTeacher);
      }
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAll: async (req, res) => {
    try {
      const myRequestGroup = await RequestChangeGroups.find()
        .sort("-createdAt")
        .populate("student teacher group groupWantChange");

      res.status(200).json(myRequestGroup);
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
};

module.exports = requestChangeGroupCtrl;
