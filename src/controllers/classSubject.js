const mongoose = require("mongoose");
const classSubjects = require("../models/ClassSubject");
const Subjects = require("../models/Subject");
const Groups = require("../models/GroupSubject");
const Users = require("../models/User");
const classSubjectCtrl = {
  insert: async (req, res) => {
    const Subjects = mongoose.model("subject");
    const Teachers = mongoose.model("user");
    try {
      const { name, subjectID, schoolYear, note, roleUser, teacherID } =
        req.body;
      if (roleUser !== "teacher" && roleUser !== "admin")
        return res
          .status(400)
          .json({ msg: "This user does not have permission to access this." });
      if (!name) return res.status(400).json({ msg: "Vui lòng điền tên" });

      const checkName = await classSubjects.findOne({ name });
      if (checkName)
        return res.status(400).json({ msg: "Đã tồn tại lớp môn học này" });

      if (!subjectID)
        return res.status(400).json({ msg: "Vui lòng điền subjectID" });

      if (!schoolYear)
        return res.status(400).json({ msg: "Vui lòng điền schoolYear" });

      if (!roleUser)
        return res.status(400).json({ msg: "Vui lòng điền roleUser" });

      if (!teacherID)
        return res.status(400).json({ msg: "Vui lòng điền teacherID" });

      const mySubject = await Subjects.findById(subjectID);

      const myTeacher = await Teachers.findById(teacherID);
      const newClassSubject = new classSubjects({
        name,
        subject: mySubject,
        schoolYear,
        note,
        teacher: myTeacher
      });

      await Subjects.findOneAndUpdate(
        {
          _id: subjectID
        },
        {
          $push: { classSubjects: newClassSubject._id }
        },
        { new: true }
      );
      5;

      await newClassSubject.save();

      const checkCSResult = await classSubjects
        .find({ name: name })
        .populate("teacher subject students");
      res.json({
        msg: `Tạo thành công lớp môn học ${name} `,
        subject: {
          ...newClassSubject._doc
        },
        myCS: checkCSResult[0]
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  addStudents: async (req, res) => {
    try {
      const Students = mongoose.model("user");
      const { classSubjectID, studentID } = req.body;

      if (classSubjectID.length <= 0)
        return res.status(400).json({ msg: "vui lòng chọn sinh viên" });

      if (!studentID)
        return res.status(400).json({ msg: "vui lòng thêm sinh viên" });

      const checkCS = await classSubjects
        .find({ _id: classSubjectID })
        .populate("teacher subject students");

      if (!checkCS)
        return res.status(400).json({ msg: "Không tồn tại lớp học này." });

      const checkStudent = await Students.find({
        _id: studentID,
        role: "student"
      });

      if (!checkStudent)
        return res
          .status(400)
          .json({ msg: "Không tồn tại sinh viên trong hệ thống !" });

      const checkStudentInCS = await classSubjects.find({
        _id: classSubjectID,
        students: studentID
      });

      if (checkStudentInCS.length > 0)
        return res.status(400).json({ msg: "Sinh viên này đã có trong lớp. " });

      await classSubjects.findOneAndUpdate(
        {
          _id: classSubjectID
        },
        {
          $push: { students: studentID }
        },
        { new: true }
      );

      await Users.findOneAndUpdate(
        {
          _id: studentID
        },
        {
          $push: { classSubjects: classSubjectID }
        },
        { new: true }
      );

      const checkCSResult = await classSubjects
        .find({ _id: classSubjectID })
        .populate("teacher subject students");

      res.status(200).json({
        msg: `Thêm ${studentID.length} sinh viên vào lớp môn học thành công !`,
        myCS: checkCSResult[0]
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteStudent: async (req, res) => {
    try {
      const Students = mongoose.model("user");
      const { classSubjectID, studentID } = req.body;

      if (!classSubjectID)
        return res.status(400).json({ msg: "vui lòng điền classSubjectID" });

      if (!studentID)
        return res.status(400).json({ msg: "vui lòng thêm sinh viên" });

      // const checkGroup = await Groups.find({ name: groupName }).populate(
      //   "members"
      // );

      // if (!checkGroup)
      //   return res.status(400).json({ msg: "Không tồn tại group này." });

      const checkStudent = await Students.findOne({
        _id: studentID
      });

      if (!checkStudent)
        return res
          .status(400)
          .json({ msg: "Không tồn tại sinh viên trong hệ thống !" });

      // const checkStudentInGroup = await Groups.find({
      //   name: groupName,
      //   members: studentID
      // });

      // if (checkStudentInGroup.length > 0)
      //   return res
      //     .status(400)
      //     .json({ msg: "Sinh viên này đã có trong group. " });

      await classSubjects.findOneAndUpdate(
        {
          _id: classSubjectID
        },
        {
          $pull: { students: studentID }
        },
        { new: true }
      );
      const checkG = await Groups.findOne({
        classSubject: classSubjectID,
        members: studentID
      }).populate("classSubject");

      if (checkG) {
        await Groups.findOneAndUpdate(
          {
            classSubject: classSubjectID,
            members: studentID
          },
          {
            $pull: { members: studentID }
          },
          { new: true }
        );

        const newGroupUpdate = await Groups.findOne({
          _id: checkG._id
        }).populate("members");

        const newCS = await classSubjects
          .find({ _id: classSubjectID })
          .populate("students teacher groups")
          .populate({
            path: "groups",
            populate: {
              path: "members"
            }
          });
        // await Users.findOneAndUpdate(
        //   {
        //     _id: studentID
        //   },
        //   {
        //     $push: { groups: groupID }
        //   },
        //   { new: true }
        // );
        res.status(200).json({
          msg: `Xóa sinh viên ${checkStudent.fullName} ra khỏi lớp ${checkG.classSubject.name} và group  ${checkG.name} thành công !`,
          myCS: newCS[0],
          group: newGroupUpdate
        });
      } else {
        const newCS = await classSubjects
          .find({ _id: classSubjectID })
          .populate("students teacher groups")
          .populate({
            path: "groups",
            populate: {
              path: "members"
            }
          });
        res.status(200).json({
          msg: `Xóa sinh viên ${checkStudent.fullName} ra khỏi lớp thành công !`,
          myCS: newCS[0]
        });
      }
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllSubjectClass: async (req, res) => {
    try {
      const classSubject = await classSubjects.find();
      res.status(200).json(classSubject);
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteSubjectClass: async (req, res) => {
    try {
      await Subjects.updateMany(
        { classSubjects: req.params.id },
        { $pull: { classSubjects: req.params.id } }
      );
      await classSubjects.findByIdAndDelete(req.params.id);
      res.status(200).json("delete seccucful");
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updateSubjectClass: async (req, res) => {
    try {
      const { newName } = req.body;

      await classSubjects.findOneAndUpdate(
        {
          _id: req.params.id
        },
        { name: newName },

        { new: true }
      );

      const newCS = await classSubjects
        .findOne({ _id: req.params.id })
        .populate("teacher groups subject ");
      res
        .status(200)
        .json({ msg: "Đổi tên lớp môn học thành công !", newCS: newCS });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getASubjectClass: async (req, res) => {
    try {
      const classSubject = await classSubjects
        .findById(req.params.id)
        .populate("groups teacher students")
        .populate({
          path: "groups",
          populate: {
            path: "members"
          }
        });
      res.status(200).json(classSubject);
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getCSByIdUser: async (req, res) => {
    try {
      let myCSs;

      const CsForStudent = await classSubjects
        .find({
          students: { _id: req.user._id }
        })
        .populate("teacher subject student");
      const CsForTeacher = await classSubjects
        .find({
          teacher: { _id: req.user._id }
        })
        .populate("teacher subject student");
      if (CsForStudent.length > 0) {
        myCSs = CsForStudent;
      } else if (CsForTeacher.length > 0) {
        myCSs = CsForTeacher;
      } else {
        myCSs = [];
      }

      res.status(200).json({
        msg: "Lấy thông tin tất cả lớp môn học cho người dùng thành công!",
        result: myCSs.length,
        myCSs
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};
module.exports = classSubjectCtrl;

// insert post cmt request request changeGr
