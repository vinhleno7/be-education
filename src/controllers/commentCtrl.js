const Comments = require("../models/Comment");
const Posts = require("../models/Post");
const commentCtrl = {
  createComment: async (req, res) => {
    try {
      const { postId, content, tag, reply } = req.body;
      if (!content)
        return res
          .status(400)
          .json({ msg: "Vui lòng nhập nội dung lời phản hồi. " });
      const newComment = new Comments({
        author: req.user._id,
        content,
        tag,
        reply,
      });

      await Posts.findOneAndUpdate(
        {
          _id: postId,
        },
        {
          $push: { comments: newComment._id },
        },
        { new: true }
      );

      await newComment.save();

      res.json({ msg: "Trả lời thành công", newComment });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  //DELETE
  delete: async (req, res) => {
    try {
      const comment = await Comments.findOneAndDelete({
        _id: req.params.id,
        $or: [{ user: req.user._id }, { postUserId: req.user._id }],
      });
      const checkPost = await Posts.findOne({
        comments: { _id: req.params.id },
      });

      await Posts.findOneAndUpdate(
        { _id: checkPost._id },
        {
          $pull: { comments: req.params.id },
        }
      );

      res.json({ msg: "Xóa câu trả lời thành công!" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  //UPDATE
  update: async (req, res) => {
    try {
      const comment = await Comments.findById(req.params.id);
      await comment.updateOne({ $set: req.body });
      res.status(200).json("Updated Successfullly");
    } catch (err) {
      res.status(500).json(err);
    }
  },
  getAllComments: async (req, res) => {
    try {
      const comment = await Comments.find();
      res.status(200).json(comment);
    } catch (err) {
      res.status(500).json(err);
    }
  },
};

module.exports = commentCtrl;
