const Majors = require("../models/Major");

const majorCtrl = {
  getAll: async (req, res) => {
    try {
      const majors = await Majors.find();

      res.json({ majors });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  insert: async (req, res) => {
    try {
      const { majorID, name } = req.body;
      const newMajor = new Majors({
        majorID,
        name
      });

      await newMajor.save();

      res.json({
        msg: "Successfully! This major has been insert ",
        major: {
          ...newMajor._doc
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  //Get An Major
  getAnMajor: async (req, res) => {
    try {
      const major = await Majors.findById(req.params.id);
      res.status(200).json(major);
    } catch (err) {
      return res.status(500).json(err);
    }
  },
  //Update Major
  updateMajor: async (req, res) => {
    try {
      const major = await Majors.findById(req.params.id);
      await major.updateOne({ $set: req.body });
      res.status(200).json("update successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  },
  //Delete Major
  deleteMajor: async (req, res) => {
    try {
      const major = await Majors.findByIdAndDelete(req.params.id);
      res.status(200).json("delete successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  }
};
module.exports = majorCtrl;
