const mongoose = require("mongoose");
const Users = require("../models/User");
const ClassSubjects = require("../models/ClassSubject");
const Requests = require("../models/Request");
const requestCtrl = {
  create: async (req, res) => {
    const { title, content, studentID, teacherID, classSubjectID } = req.body;
    try {
      // if (!title) return res.status(400).json({ msg: "vui lòng điền title" });
      // if (!content)
      return res.status(400).json({ msg: "vui lòng điền content" });
      if (!studentID)
        return res.status(400).json({ msg: "vui lòng điền studentID" });
      if (!teacherID)
        return res.status(400).json({ msg: "vui lòng điền teacherID" });
      if (!classSubjectID)
        return res.status(400).json({ msg: "vui lòng điền classSubjectID" });
      const myStudent = await Users.findById(studentID);
      if (myStudent.length <= 0)
        return res.status(404).json({ msg: "Không tồn tại sinh viên này" });
      const myTeacher = await Users.findById(teacherID);
      if (myTeacher.length <= 0)
        return res.status(404).json({ msg: "Không tồn tại giáo viên này" });
      const myClassSubject = await ClassSubjects.findById(classSubjectID);
      if (myStudent.length <= 0)
        return res.status(404).json({ msg: "Không tồn tại class subject này" });

      const newRequest = new Requests({
        // title,
        content,
        classSubject: myClassSubject,
        student: myStudent,
        teacher: myTeacher,
      });

      await newRequest.save();

      res.json({
        msg: "Created Request!",
        Request: {
          ...newRequest._doc,
        },
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllRequest: async (req, res) => {
    try {
      const requests = await Requests.find();
      res.status(200).json(requests);
    } catch (err) {
      res.status(500).json(err);
    }
  },
};

module.exports = requestCtrl;
