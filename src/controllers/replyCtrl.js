const mongoose = require("mongoose");
const Users = require("../models/User");
const ClassSubjects = require("../models/ClassSubject");
const Requests = require("../models/Request");
const Replies = require("../models/Reply");
const replyCtrl = {
  create: async (req, res) => {
    const { content, authorID, requestID } = req.body;
    try {
      if (!content)
        return res.status(400).json({ msg: "vui lòng điền content" });
      if (!authorID)
        return res.status(400).json({ msg: "vui lòng điền authorID" });

      const myRequest = await Requests.findById(requestID);
      if (myRequest.length <= 0)
        return res.status(404).json({ msg: "Không tồn tại request này" });
      const myAuthor = await Users.findById(authorID);
      if (myAuthor.length <= 0)
        return res.status(404).json({ msg: "Không tồn tại author này" });
      const checkAuthor = await Requests.find({
        _id: requestID,
        teacher: { _id: authorID }
      });

      const checkAuthor2 = await Requests.find({
        _id: requestID,
        student: { _id: authorID }
      });
      if (checkAuthor.length <= 0 && checkAuthor2.length <= 0)
        return res.status(400).json({ msg: "author này không có quyền reply" });
      const newReply = new Replies({
        content,
        author: myAuthor,
        requestID: myRequest
      });

      await Requests.findOneAndUpdate(
        {
          _id: requestID
        },
        {
          $push: { replies: newReply._id }
        },
        { new: true }
      );

      await newReply.save();

      res.status(200).json({ msg: "Reply thành công. ", newReply });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  //DELETE
  delete: async (req, res) => {
    try {
      await Requests.updateMany(
        {replies: req.params.id},
        {$pull: {replies: req.params.id}}
        );
     await Replies.findByIdAndDelete(req.params.id);
      res.status(200).json("delete successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  },
  //UPDATE
  update: async (req, res) => {
    try{
      const replies = await Replies.findById(req.params.id);
      await replies.updateOne({ $set: req.body });
      res.status(200).json("Updated Successfullly");
    }catch (err) {
      res.status(500).json(err);
    }

  }


};

module.exports = replyCtrl;
