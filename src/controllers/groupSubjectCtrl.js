const mongoose = require("mongoose");
const Groups = require("../models/GroupSubject");
const classSubjects = require("../models/ClassSubject");
const Users = require("../models/User");
const groupSubjectCtrl = {
  insertGroup: async (req, res) => {
    try {
      const { name, classSubjectID } = req.body;

      if (!name)
        return res.status(400).json({ msg: "Vui lòng điền tên group" });

      const checkName = await Groups.findOne({ name });
      if (checkName)
        return res.status(400).json({ msg: "Đã tồn tại group này" });

      if (!classSubjectID)
        return res.status(400).json({ msg: "Vui lòng điền classSubjectID" });

      const CSs = mongoose.model("classSubject");
      const myCSs = await CSs.findById(classSubjectID);

      if (!myCSs)
        return res
          .status(400)
          .json({ msg: "classSubject không tồn tại trong hệ thống !" });

      const newGroup = new Groups({
        name,
        classSubject: myCSs
      });

      await classSubjects.findOneAndUpdate(
        {
          _id: classSubjectID
        },
        {
          $push: { groups: newGroup._id }
        },
        { new: true }
      );

      await newGroup.save();

      res.json({
        msg: `Tạo thành công nhóm ${name}`,
        group: {
          ...newGroup._doc
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  addMembers: async (req, res) => {
    try {
      const Students = mongoose.model("user");
      const { groupName, studentID } = req.body;

      if (!groupName)
        return res.status(400).json({ msg: "vui lòng điền groupName" });

      if (!studentID)
        return res.status(400).json({ msg: "vui lòng thêm sinh viên" });

      const checkGroup = await Groups.find({ name: groupName }).populate(
        "members"
      );

      if (!checkGroup)
        return res.status(400).json({ msg: "Không tồn tại group này." });

      const checkStudent = await Students.find({
        _id: studentID,
        role: "student"
      });

      if (!checkStudent)
        return res
          .status(400)
          .json({ msg: "Không tồn tại sinh viên trong hệ thống !" });

      const checkStudentInGroup = await Groups.find({
        name: groupName,
        members: studentID
      });

      if (checkStudentInGroup.length > 0)
        return res
          .status(400)
          .json({ msg: "Sinh viên này đã có trong group. " });

      await Groups.findOneAndUpdate(
        {
          name: groupName
        },
        {
          $push: { members: studentID }
        },
        { new: true }
      );
      const newGroup = await Groups.find({ name: groupName }).populate(
        "members"
      );
      // await Users.findOneAndUpdate(
      //   {
      //     _id: studentID
      //   },
      //   {
      //     $push: { groups: groupID }
      //   },
      //   { new: true }
      // );
      res.status(200).json({
        msg: "Thêm sinh viên vào group thành công !",
        group: newGroup[0]
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteMember: async (req, res) => {
    try {
      const Students = mongoose.model("user");
      const { groupName, studentID } = req.body;

      if (!groupName)
        return res.status(400).json({ msg: "vui lòng điền groupName" });

      if (!studentID)
        return res.status(400).json({ msg: "vui lòng thêm sinh viên" });

      const checkGroup = await Groups.find({ name: groupName }).populate(
        "members"
      );

      if (!checkGroup)
        return res.status(400).json({ msg: "Không tồn tại group này." });

      const checkStudent = await Students.findOne({
        _id: studentID,
        role: "student"
      });

      if (!checkStudent)
        return res
          .status(400)
          .json({ msg: "Không tồn tại sinh viên trong hệ thống !" });

      // const checkStudentInGroup = await Groups.find({
      //   name: groupName,
      //   members: studentID
      // });

      // if (checkStudentInGroup.length > 0)
      //   return res
      //     .status(400)
      //     .json({ msg: "Sinh viên này đã có trong group. " });

      await Groups.findOneAndUpdate(
        {
          name: groupName
        },
        {
          $pull: { members: studentID }
        },
        { new: true }
      );
      const newGroup = await Groups.find({ name: groupName }).populate(
        "members"
      );
      // await Users.findOneAndUpdate(
      //   {
      //     _id: studentID
      //   },
      //   {
      //     $push: { groups: groupID }
      //   },
      //   { new: true }
      // );
      res.status(200).json({
        msg: `Xóa sinh viên ${checkStudent.fullName} ra khỏi group thành công !`,
        group: newGroup[0]
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getAllGroup: async (req, res) => {
    try {
      const groups = await Groups.find();
      res.status(200).json(groups);
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteGroup: async (req, res) => {
    try {
      const checkGroup = await Groups.findOne({ _id: req.params.id });
      if (!checkGroup)
        return res.status(400).json({ msg: "Nhóm này đã không còn nữa. " });
      await classSubjects.updateMany(
        { groups: req.params.id },
        { $pull: { groups: req.params.id } }
      );
      await Groups.findByIdAndDelete(req.params.id);
      res.status(200).json({ msg: `Xoá nhóm ${checkGroup.name} thành công.` });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updateGroup: async (req, res) => {
    try {
      const { newName, groupID } = req.body;
      const checkGroupID = await Groups.findOne({ _id: groupID });
      if (!checkGroupID)
        return res.status(400).json({ msg: "Nhóm này đã không còn tồn tại." });

      if (newName === checkGroupID.name)
        return res.status(400).json({ msg: "Tên trùng với tên cũ. " });

      await Groups.findOneAndUpdate(
        {
          _id: groupID
        },
        {
          name: newName
        },
        { new: true }
      );
      const newGroup = await Groups.find({ _id: groupID }).populate("members");

      res.status(200).json({
        msg: `Cập nhật tên nhóm [ ${checkGroupID.name} -> ${newName} ] thành công!`,
        group: newGroup[0]
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

  getAGroup: async (req, res) => {
    try {
      const groups = await Groups.findById(req.params.id).populate(
        "classSubject"
      );
      res.status(200).json(groups);
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};
module.exports = groupSubjectCtrl;
