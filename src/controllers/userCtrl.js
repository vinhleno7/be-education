const mongoose = require("mongoose");
const Users = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const userCtrl = {
  getAllUser: async (req, res) => {
    try {
      const users = await Users.find()
        .select("-password")
        .populate("class major");

      res.json({ users });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getUser: async (req, res) => {
    try {
      const user = await Users.findById(req.params.id)
        .select("-password")
        .populate("class major");
      if (!user) return res.status(400).json({ msg: "User does not exist." });

      res.json({ user });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteUser: async (req, res) => {
    try {
      await Users.deleteOne({ _id: req.params.id });

      res.json({ msg: "Delete Success !" });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updateUser: async (req, res) => {
    try {
      const {
        avatar,
        fullName,
        mobile,
        website,
        gender,
        password,
        majorID,
        classID
      } = req.body;
      const Classes = mongoose.model("class");
      const Majors = mongoose.model("major");
      if (majorID) {
        const checkMajor = await Majors.findOne({ _id: majorID });
        if (!checkMajor)
          return res
            .status(400)
            .json({ msg: "Không có ngành này trong hệ thống." });
      }
      if (classID) {
        const checkClass = await Classes.findOne({ _id: classID });
        if (!checkClass)
          return res
            .status(400)
            .json({ msg: "Không có lớp này trong hệ thống." });
      }

      if (!fullName)
        return res.status(400).json({ msg: "Please enter your full name." });

      const passwordHash = await bcrypt.hash(password, 12);
      await Users.findOneAndUpdate(
        { _id: req.user._id },
        {
          avatar,
          fullName,
          mobile,
          website,
          gender,
          password: passwordHash,
          class: classID,
          major: majorID
        }
      );
      const myUser = await Users.findOne({ _id: req.user._id }).populate(
        "major class"
      );

      res.json({ msg: "Update Success !", user: myUser });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
};

module.exports = userCtrl;
