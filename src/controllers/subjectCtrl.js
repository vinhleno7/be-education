const mongoose = require("mongoose");
const Subjects = require("../models/Subject");

const subjectCtrl = {
  insert: async (req, res) => {
    const Majors = mongoose.model("major");

    try {
      const { name, majorID, schoolYear, note } = req.body;
      const myMajor = await Majors.findById(majorID);
      const newSubject = new Subjects({
        name,
        major: myMajor,
        schoolYear,
        note
      });

      await newSubject.save();

      res.json({
        msg: "Successfully! This subject has been insert ",
        subject: {
          ...newSubject._doc
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }, ///GET ALL SUBJECT
  getAllSubject: async (req, res) => {
    try {
      const subjects = await Subjects.find();
      res.status(200).json(subjects);
    } catch (err) {
      res.status(500).json(err);
    }
  },
  //GET AN SUBJECT
  getASubject: async (req, res) => {
    try {
      const subjects = await Subjects.findById(req.params.id);
      res.status(200).json(subjects);
    } catch (err) {
      res.status(500).json(err);
    }
  },
  /// DELETE
  delete: async (req, res) => {
    try {
      const subject = await Subjects.findByIdAndDelete(req.params.id);
      res.status(200).json("delete successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  },
  ///UPDATE
  update: async (req, res) => {
    try {
      const subject = await Subjects.findById(req.params.id);
      await subject.updateOne({ $set: req.body });
      res.status(200).json("update successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  }
};
module.exports = subjectCtrl;
