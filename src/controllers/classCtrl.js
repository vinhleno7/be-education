const mongoose = require("mongoose");
const Classes = require("../models/class");

const classCtrl = {
  getAllClass: async (req, res) => {
    try {
      const classes = await Classes.find();

      res.json({ classes });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  insert: async (req, res) => {
    const Majors = mongoose.model("major");
    try {
      const { name, majorID } = req.body;

      if (!name) return res.status(400).json({ msg: "Vui lòng điền tên lớp" });

      let newName = name.toLowerCase().replace(/ /g, "");
      const checkClassName = await Classes.findOne({ newName });
      if (checkClassName)
        return res.status(400).json({ msg: "Đã tồn tại lớp này" });

      if (!majorID) return res.status(400).json({ msg: "Vui lòng điền ngành" });

      const myMajor = await Majors.findById(majorID);

      if (!myMajor)
        return res
          .status(400)
          .json({ msg: "Không tồn tại ngành này trong hệ thống." });
      const newClass = new Classes({
        name,
        major: myMajor
      });

      await newClass.save();

      res.json({
        msg: "Successfully! This Class has been insert ",
        class: {
          ...newClass._doc
        }
      });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  //Get An Class
  getAnClass: async (req, res) => {
    try {
      const classes = await Classes.findById(req.params.id);

      res.json({ classes });
    } catch (err) {
      return res.status(500).json(err);
    }
  },

  //Delete class
  deleteClass: async (req, res) => {
    try {
      const classes = await Classes.findByIdAndDelete(req.params.id);
      res.status(200).json("delete successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  },
  //Update Class
  updateClass: async (req, res) => {
    try {
      const classes = await Classes.findById(req.params.id);
      await classes.updateOne({ $set: req.body });
      res.status(200).json("update successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  }
};
module.exports = classCtrl;
