const mongoose = require("mongoose");
const connectDB = async () => {
  try {
    await mongoose.connect(
      `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.e9tu7.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`,
      {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );
    console.log("🚀 MONGODB CONNECT SUCCESSFUL 🚀");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};
module.exports = connectDB;
