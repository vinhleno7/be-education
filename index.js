require("dotenv").config();

const express = require("express");
// const authRouter = require("./src/routes/auth");
// const postRouter = require("./src/routes/post");
const connectDB = require("./src/configs/db.config");
const cors = require("cors");
require("dotenv").config();
const cookieParser = require("cookie-parser");

const app = express();

//connect to db
connectDB();
app.use(express.json());
app.use(cors());
app.use(cookieParser());

app.get("/", (req, res) => {
  res.json({ msg: "This is my server [LV Social]" });
});

// Routes
app.use("/api", require("./src/routes/auth"));
app.use("/api", require("./src/routes/user"));
app.use("/api/class", require("./src/routes/class"));
app.use("/api/major", require("./src/routes/major"));
app.use("/api/subject", require("./src/routes/subject"));
app.use("/api/classSubject", require("./src/routes/classSubject"));
app.use("/api/groupSubject", require("./src/routes/groupSubject"));
app.use("/api/post", require("./src/routes/post"));
app.use("/api/comment", require("./src/routes/comment"));
app.use("/api/requestChangeGroup", require("./src/routes/requestChangeGroup"));
app.use("/api/request", require("./src/routes/request"));
app.use("/api/reply", require("./src/routes/reply"));
app.use("/api/group", require("./src/routes/groupSubject"));
const PORT = process.env.PORT || 5001;

app.listen(PORT, () => console.log(`🚀 Server started on ${PORT} 🚀`));
